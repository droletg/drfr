# drfr

Ce package est une bo�te � outils contenant des fonctions et des classes utiles aux diff�rents projets de recherche de la Direction de la recherche foresti�re (DRF) du Minist�re des For�ts, de la Faune et des Parcs (MFFP) du Qu�bec.


## Installation

Deux approches pour installer le package :

```R
# 1) 
# T�l�charger le package � partir de la section Downloads du r�f�rentiel 
# Bitbucket du package et :
install.packages("/chemin/vers/le/package/drfr_XXXX.zip", repos = NULL,
  dependencies = TRUE)

# 2)
# Installer Rtools sous Windows
# Installer le package devtools et :
devtools::install_bitbucket("droletg/drfr", dependencies = TRUE)

```


