#' Excel to Shapefile
#'
#' Convertit un fichier Excel (.xlsx) en fichier ESRI Shapefile (.shp)
#'
#' Cette fonction convertit les fichiers Excel 2007+ (.XLSX) contenant
#' des points sous forme de coordonn�es g�ographiques (XY) en fichier spatial de
#' type ESRI Shapefile (.shp, .shx, .dbf, ...)
#'
#' @param xlsx_file Character (obligatoire). Nom du fichier Excel. C'est un nom
#'   absolu ou relatif: dans ce dernier cas il doit pouvoir �tre accessible �
#'   la session R ou un message d'erreur seras �mis.
#' @param coords Character (obligatoire). Un vecteur contenant deux valeurs de
#'   type \emph{caract�re} et qui sp�cifie les noms des colonnes du fichier
#'   \code{xlsx_file} qui contiennent les coordonn�es en X (longitude) et en Y
#'   (latitude), dans cet ordre. Les coordonn�es doivent �tre en degr�s
#'   d�cimaux.
#' @param shape_file Character (optionnel). Le nom du fichier de sortie, avec
#'   l'extension \emph{.shp}. Si c'est argument n'est pas fourni, le fichier de
#'   sortie aura le m�me nom que le fichier \code{xlsx_file} mais avec
#'   l'extension \emph{.shp}.
#' @param sheet Numeric (optionnel). Le num�ro de la feuille Excel qui contient
#'   les donn�es avec les coordonn�es. La valeur par d�faut est 1, soit la
#'   premi�re feuille du classeur Excel.
#' @param out_dir Character (optionnel). Le nom du r�pertoire dans lequel sera
#'   enregistr� le fichier de sortie (i.e. \code{shape_file}). Si ce r�pertoire
#'   n'existe pas, il sera cr�e. Si cet argument n'est pas sp�cifi�, le fichier
#'   de sortie sera enregistr� dans le r�pertoire qui contient \code{xlsx_file}.
#' @param layer Character (optionnel). Le nom de la couche � cr�er dans le
#'   fichier de sortie. Si non sp�cifi�, le nom de la couche sera le nom du
#'   fichier Excel sans l'extension (.XLSX). Il peut �tre pratique de sp�cifier
#'   cet argument lorsque plusieurs fichiers sont cr��s � partir des diff�rentes
#'   feuilles du classeur Excel (voir exemples).
#' @param crs Numeric (optionnel). Le code EPSG du syst�me de coordonn�es
#'   g�ographique des coordonn�es de points. La valeur par d�faut est
#'   4326 (WGS84).
#' @param encoding Character (optionnel). Encodage � utiliser pour le fichier de
#'   sortie (e.g., "UTF-8"). Si non sp�cifi�, l'encodage de sortie sera celui
#'   du syst�me.
#' @param verbose Logical (optionnel). Si cette valeur �gale TRUE (d�faut), un
#'   message contenant des d�tails sur le fichier cr�� sera �mis.
#' @return Le fichier Excel d'entr�e est converti en fichier shapefile et est
#'   enregistr� dans le r�pertoire \code{out_dir} ou dans le r�pertoire courant
#'   si non sp�cifi�. Un message d'erreur si un probl�me survient. Un objet de
#'   type \code{sf} contenant les points et leurs attributs est aussi retourn�.
#' @examples
#' fichier_xlsx <- system.file("extdata", "foret_exp.xlsx", package = "drfr")
#' out_dir <- dirname(fichier_xlsx)
#' encoding = "UTF-8"
#'
#' # les noms des colonnes du fichier Excel qui contiennent les coordonn�es
#' # des points (en degr�s d�cimaux)
#' coords = c("LONG_DD", "LAT_DD")
#'
#' xlsx_to_shp(fichier_xlsx, out_dir = out_dir, coords = coords, verbose = FALSE)
#'
#' # classeur Excel avec 2 feuilles contenant des coordonn�es. Les noms de
#' # colonnes contenant les coordonn�es sont les m�me dans les deux feuilles.
#' lapply(1:2, function(x) {
#'   xlsx_to_shp(fichier_xlsx, out_dir = out_dir, coords = coords,
#'     shape_file = paste0("shape_", x, ".shp"), sheet = x,
#'     layer = paste0("sheet_", x), verbose = FALSE)
#' })
#' @seealso
#'   Code EPSG: \href{http://spatialreference.org}{spatialreference.org}. \cr
#'   Options du pilote ESRI Shapefile (Encoding):
#'     \url{http://www.gdal.org/drv_shapefile.html}.
#' @export
xlsx_to_shp <- function(xlsx_file, coords = c("x", "y"), shape_file, sheet = 1,
  out_dir = dirname(xlsx_file), layer = NA_character_, crs = 4326,
  encoding = NA_character_, verbose = TRUE) {

  if (!file.exists(xlsx_file)) stop("Le fichier Excel n'existe pas.")

  invisible(ifelse(!file.exists(out_dir), dir.create(out_dir), NA))

  tryCatch({

    xlsx_data <- rio::import(xlsx_file, which = sheet, readxl = FALSE)

    stopifnot(all(coords %in% names(xlsx_data)))

    xlsx_sf <- sf::st_as_sf(xlsx_data, coords = coords, crs = crs,
      relation_to_geometry = "field")

    lco <- c("RESIZE=YES", "SHPT=POINT")
    if (!is.na(encoding)) lco <- c(lco, paste0("ENCODING=", encoding))

    if (missing(shape_file)) {
      layer <- strsplit(basename(xlsx_file), "\\.")[[1]][1]
      shape_file <- file.path(out_dir, paste0(layer, ".shp"))
    } else {
      if (!grepl("\\.shp$", shape_file))
        stop("Le nom du fichier shapefile n'a pas l'extension .shp.")

      shape_file <- file.path(out_dir, basename(shape_file))
      if (is.na(layer)) {
        layer <- strsplit(basename(shape_file), split = ".",
          fixed = TRUE)[[1]][1]
      }
    }
    sf::st_write(xlsx_sf, shape_file, layer = layer, layer_options = lco,
      quiet = !verbose)
    return(xlsx_sf)
  }, error = function(err) {
    cat("Une erreur s'est produite en convertissant le fichier Excel:", err)
    unlink(shape_file)
  })
}
