#' drfr.
#'
#' Outils R pour la Direction de la recherche foresti�re (DRF)
#'
#' Ce package R contient des fonctions, des classes et des m�thodes utiles pour
#' les activit�s et projets de recherche de la DRF. La collection d'outils
#' grossira � mesure des besoins des diff�rent projets et analyses.
#'
#' @name drfr
#' @docType package
NULL
